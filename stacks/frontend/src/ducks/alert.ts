import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import { isAbortOrcidVerificationAction, isFailOrcidVerificationAction } from './account';
import { FluxStandardAction } from './app';

export interface AlertState {
  error?: string | JSX.Element;
  success?: string | JSX.Element;
  warning?: string | JSX.Element;
}

const CompleteSomethingActionType: 'flockademic/alert/completeSomething' = 'flockademic/alert/completeSomething';
export interface CompleteSomethingAction extends FluxStandardAction {
  type: 'flockademic/alert/completeSomething';
  payload: { successMessage: string | JSX.Element; };
}

const IgnoreCompleteSomethingActionType: 'flockademic/alert/ignoreCompleteSomething'
  = 'flockademic/alert/ignoreCompleteSomething';
export interface IgnoreCompleteSomethingAction extends FluxStandardAction {
  type: 'flockademic/alert/ignoreCompleteSomething';
  payload: { successMessage: string | JSX.Element; };
}

export function isCompleteSomethingAction(action: FluxStandardAction): action is CompleteSomethingAction {
  return action.type === CompleteSomethingActionType;
}

export function isIgnoreCompleteSomethingAction(action: FluxStandardAction): action is IgnoreCompleteSomethingAction {
  return action.type === IgnoreCompleteSomethingActionType;
}
const initialState: AlertState = {};

export function reducer(prevState: AlertState = initialState, action: FluxStandardAction): AlertState {
  if (isFailOrcidVerificationAction(action)) {
    return { error: action.payload.error.message };
  }
  if (isAbortOrcidVerificationAction(action)) {
    return { error: 'Please approve access to your ORCID to link it.' };
  }

  if (isCompleteSomethingAction(action)) {
    return { success: action.payload.successMessage };
  }

  if (isIgnoreCompleteSomethingAction(action) && prevState.success === action.payload.successMessage) {
    return { success: undefined };
  }

  // When the user performs an action, messages can be hidden again:
  return initialState;
}

export function completeSomething(successMessage: string | JSX.Element)
: ThunkAction<void, AlertState, {}, CompleteSomethingAction | IgnoreCompleteSomethingAction> {
  return async (dispatch: ThunkDispatch<AlertState, {}, CompleteSomethingAction | IgnoreCompleteSomethingAction>) => {
    const completion: CompleteSomethingAction = {
      payload: { successMessage },
      type: CompleteSomethingActionType,
    };
    dispatch(completion);

    setTimeout(
      () => {
        const ignore: IgnoreCompleteSomethingAction = {
          payload: { successMessage },
          type: IgnoreCompleteSomethingActionType,
        };
        dispatch(ignore);
      },
      5000,
    );
  };
}
