jest.mock('react-ga');

import { NotFound } from '../../src/components/notFound/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot', () => {
  const quip = shallow(<NotFound match={{ url: 'https://arbitrary_url' }}/>);

  expect(toJson(quip)).toMatchSnapshot();
});

it('should send an analytics event when ending up at the 404 page', () => {
  const mockedReactGa = require.requireMock('react-ga');
  const quip = shallow(<NotFound match={{ url: 'https://arbitrary_url' }}/>);

  expect(mockedReactGa.event.mock.calls.length).toBe(1);
  expect(mockedReactGa.event.mock.calls[0][0]).toEqual({
    action: 'Not found',
    category: '404 Page Not Found',
  });
});
